import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { pluck } from 'rxjs/operators';

import { CdnService } from '../cdn/cdn.service';
import { Config } from '../config';
import { PROFILE_KEY } from '../session.constants';
import { ToasterService } from '../toaster.service';
import { BusinessProfile } from './models/business/businessProfile';
import { CreateBusinessAndProfile } from './models/business/createBusinessAndProfile';
import { BusinessData } from './models/businessData';
import { InviteData } from './models/invite/inviteData';
import { InviteResponseData } from './models/invite/inviteResponseData';
import { Profile } from './models/profile/profile';
import { Role } from './models/role';
import { StringTag } from './models/stringTag';
import { User } from './models/user';

@Injectable({
  providedIn: 'root',
})
export class BusinessService {
  private config: Config = new Config();
  private businessProfilesSubject = new BehaviorSubject<BusinessProfile[]>([]);

  constructor(
    private httpClient: HttpClient,
    private cdnService: CdnService,
    private toasterService: ToasterService,
    private router: Router,
    @Optional() config?: Config
  ) {
    if (config) {
      this.config = config;
    }
  }

  public addAccountToBusinessProfile(businessId: string, accountId: string, role: Role): void {
    this.httpClient
      .post(`${this.config.apiUrl}/profiles/business/${businessId}/account/${accountId}`, { role })
      .subscribe(() => {
        this.toasterService.showToast('Yea!', 'User added to your business', 'success');
      });
  }

  public deleteAccountFromBusinessProfile(businessId: string, accountId?: string): Observable<Subscription> {
    const url = accountId
      ? `${this.config.apiUrl}/profiles/business/${businessId}/account/${accountId}`
      : `${this.config.apiUrl}/profiles/business/${businessId}/account`;

    return of(
      this.httpClient.delete(url).subscribe(() => {
        const msg = accountId ? 'User deleted successfully' : 'You left the business!';
        this.toasterService.showToast('Bye', msg, 'info');
      })
    );
  }

  public getAllUsersFromBusinessProfile(businessId: string): Observable<User[]> {
    return this.httpClient.get(`${this.config.apiUrl}/profiles/business/${businessId}/users`).pipe(pluck('users'));
  }

  public getAllBusinessProfiles(): Observable<BusinessProfile[]> {
    this.httpClient
      .get(`${this.config.apiUrl}/profiles/business/all`)
      .pipe(pluck('businessProfiles'))
      .subscribe((businessProfiles: BusinessProfile[]) => {
        this.businessProfilesSubject.next(businessProfiles);
      });

    return this.businessProfilesSubject.asObservable();
  }

  public getBusinessByName(name: string): Observable<BusinessProfile> {
    return this.httpClient.get(`${this.config.apiUrl}/profiles/business/name/${name}`).pipe(pluck('businessProfile'));
  }

  public getBusinessById(id: string): Observable<BusinessProfile> {
    return this.httpClient.get(`${this.config.apiUrl}/profiles/business/${id}`).pipe(pluck('businessProfile'));
  }

  public async createBusinessAndProfile(data: BusinessData, accountId?: string) {
    if (data.file) {
      const serviceable = await this.cdnService.uploadUniqueServiceable(
        {
          data: data.file,
          mimeType: data.file.type,
          name: data.businessProfile.businessName,
        },
        data.businessProfile.businessName
      );
      data.businessProfile.stringTags.push(new StringTag('logoImageName', serviceable.tag));
    }

    const reqModel = new CreateBusinessAndProfile(
      Role.ALL,
      data.businessProfile.businessName,
      data.businessProfile.stringTags,
      data.businessProfile.doubleTags,
      data.businessProfile.ownerIds,
      [],
      []
    );

    this.httpClient
      .post(`${this.config.apiUrl}/profiles/profile/business/${accountId ? 'account/' + accountId : ''}`, reqModel)
      .pipe(pluck('profile'))
      .subscribe((profile: Profile) => {
        sessionStorage.setItem(PROFILE_KEY, JSON.stringify(profile));
        const businessProfiles = this.businessProfilesSubject.value;
        businessProfiles.push(profile.businessProfile);
        this.businessProfilesSubject.next(businessProfiles);
        this.toasterService.showToast('Success', 'Created business successfully', 'success');
      });
  }

  public delete(id: string, logoImage?: string): Observable<BusinessProfile[]> {
    this.httpClient.delete(`${this.config.apiUrl}/profiles/business/${id}`).subscribe(() => {
      if (logoImage) {
        this.cdnService.deleteServiceable(logoImage);
      }
      this.toasterService.showToast('Bye', 'Deleted successfully!', 'info');
      this.businessProfilesSubject.next(
        this.businessProfilesSubject.value.filter((businessProfile) => businessProfile.businessProfileId !== id)
      );
      sessionStorage.clear();
    });

    return this.businessProfilesSubject.asObservable();
  }

  public getBusinessByInviteId(id: string): Observable<BusinessProfile> {
    return this.httpClient.get(`${this.config.apiUrl}/profiles/business/invite/${id}`).pipe(pluck('businessProfile'));
  }

  public invite(data: InviteData): void {
    this.httpClient.post(`${this.config.apiUrl}/profiles/business/invite`, data).subscribe(() => {
      this.toasterService.showToast('Yes!', 'Invitation successfully send.', 'success');
    });
  }

  public accept(data: InviteResponseData, successRedirect: string): void {
    this.httpClient.post(`${this.config.apiUrl}/profiles/business/invite/accept`, data).subscribe(() => {
      this.router.navigateByUrl(successRedirect);
      this.toasterService.showToast('Attention!', 'Invitation has been accepted!', 'info');
    });
  }

  public decline(data: InviteResponseData, successRedirect: string): void {
    this.httpClient.post(`${this.config.apiUrl}/profiles/business/invite/decline`, data).subscribe(() => {
      this.router.navigateByUrl(successRedirect);
      this.toasterService.showToast('Attention!', 'Invitation has been declined!', 'info');
    });
  }
}
