import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

import { Profile } from './models/profile/profile';
import { UpdateBusinessProfile } from './models/business/updateBusinessProfile';
import { BusinessProfile } from './models/business/businessProfile';
import { PROFILE_KEY } from '../session.constants';
import { Config } from '../config';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  private config: Config = new Config();
  private profileSubject = new BehaviorSubject<Profile>(null);

  constructor(private httpClient: HttpClient, @Optional() config?: Config) {
    if (config) {
      this.config = config;
    }
  }

  get getProfileSubject() {
    return this.profileSubject.asObservable();
  }

  public getProfileByAccountId(): Observable<Profile> {
    if (sessionStorage.getItem(PROFILE_KEY)) {
      const profile: Profile = JSON.parse(sessionStorage.getItem(PROFILE_KEY));
      this.profileSubject.next(profile);
    }
    this.httpClient
      .get(`${this.config.apiUrl}/profiles/my`)
      .pipe(pluck('profile'))
      .subscribe((profile: Profile) => {
        if (profile && profile.profileId != null) {
          sessionStorage.setItem(PROFILE_KEY, JSON.stringify(profile));
        }
        this.profileSubject.next(profile);
      });

    return this.profileSubject.asObservable();
  }

  public getProfileFromSession(): Profile {
    if (sessionStorage.getItem(PROFILE_KEY)) {
      return JSON.parse(sessionStorage.getItem(PROFILE_KEY));
    }
  }

  public updateBusiness(id: string, reqModel: UpdateBusinessProfile): Observable<Profile> {
    this.httpClient
      .put(`${this.config.apiUrl}/profiles/business/${id}`, reqModel)
      .pipe(pluck('businessProfile'))
      .subscribe((business: BusinessProfile) => {
        const profile = this.profileSubject.value;
        profile.businessProfile = business;
        sessionStorage.setItem(PROFILE_KEY, JSON.stringify(profile));
        this.profileSubject.next(profile);
      });

    return this.profileSubject.asObservable();
  }
}
