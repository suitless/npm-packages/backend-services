import { ModuleReport } from './moduleReport';
import { Todo } from './todo';
import { BusinessProfile } from '../business/businessProfile';
import { Role } from '../role';
import { StringTag } from '../stringTag';
import { DoubleTag } from '../doubleTag';

export class Profile {
  profileId: string;
  accountId: string;
  businessProfile: BusinessProfile;
  roles: Role[];
  moduleReports: ModuleReport[];
  todos: Todo[];
  stringTags: StringTag[];
  doubleTags: DoubleTag[];
}
