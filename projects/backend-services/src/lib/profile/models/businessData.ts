import { BusinessProfile } from './business/businessProfile';

export class BusinessData {
  businessProfile: BusinessProfile;
  file: File;
}
