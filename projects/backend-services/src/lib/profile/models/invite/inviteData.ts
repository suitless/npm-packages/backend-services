import { BusinessProfile } from '../business/businessProfile';

export class InviteData {
  email: string;
  businessProfile: BusinessProfile;

  constructor(email: string, businessProfile: BusinessProfile) {
    this.email = email;
    this.businessProfile = businessProfile;
  }
}
