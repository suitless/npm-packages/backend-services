export class InviteResponseData {
  email: string;
  businessId: string;

  constructor(email: string, businessId: string) {
    this.email = email;
    this.businessId = businessId;
  }
}
