import { DoubleTag } from '../doubleTag';
import { Role } from '../role';
import { StringTag } from '../stringTag';

export class CreateBusinessAndProfile {
  role: Role;
  businessName: string;
  ownerIds: string[];
  businessStringTags: StringTag[];
  businessDoubleTags: DoubleTag[];
  stringTags: StringTag[];
  doubleTags: DoubleTag[];

  constructor(
    role: Role,
    businessName: string,
    businessStringTags?: StringTag[],
    businessDoubleTags?: DoubleTag[],
    ownerIds?: string[],
    stringTags?: StringTag[],
    doubleTags?: DoubleTag[]
  ) {
    this.role = role;
    this.businessName = businessName;
    this.ownerIds = ownerIds;
    this.businessStringTags = businessStringTags;
    this.businessDoubleTags = businessDoubleTags;
    this.stringTags = stringTags;
    this.doubleTags = doubleTags;
  }
}
