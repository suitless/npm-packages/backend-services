import { StringTag } from '../stringTag';
import { DoubleTag } from '../doubleTag';

export class UpdateBusinessProfile {
  businessName: string;
  ownerIds: string[];
  stringTags: StringTag[];
  doubleTags: DoubleTag[];

  constructor(businessName: string, ownerIds: string[], stringTags?: StringTag[], doubleTags?: DoubleTag[]) {
    this.businessName = businessName;
    this.ownerIds = ownerIds;
    this.stringTags = stringTags;
    this.doubleTags = doubleTags;
  }
}
