import { BusinessProfile } from './businessProfile';

export class BusinessProfileData {
  businessName: string;
  amountOfEmployees: number;
  businessEntity: string;
  businessModel: string;
  commercialTransaction: string;
  logoImageName: string;

  public static getData(businessProfile: BusinessProfile): BusinessProfileData {
    const { businessName } = businessProfile;

    // tslint:disable-next-line: one-variable-per-declaration
    let businessEntity,
      businessModel,
      commercialTransaction,
      logoImageName = '',
      amountOfEmployees = 0;

    if (businessProfile.stringTags) {
      businessEntity = businessProfile.stringTags.find((t) => t.key === 'businessEntity').value;
      businessModel = businessProfile.stringTags.find((t) => t.key === 'businessModel').value;
      commercialTransaction = businessProfile.stringTags.find((t) => t.key === 'commercialTransaction').value;
      const logoResult = businessProfile.stringTags.find((t) => t.key === 'logoImageName');
      if (logoResult) {
        logoImageName = logoResult.value;
      }
    }

    if (businessProfile.doubleTags) {
      amountOfEmployees = +businessProfile.doubleTags.find((t) => t.key === 'amountOfEmployees').value;
    }

    return { businessName, amountOfEmployees, businessEntity, businessModel, commercialTransaction, logoImageName };
  }
}
