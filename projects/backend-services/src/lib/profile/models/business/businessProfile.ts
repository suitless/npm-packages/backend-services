import { StringTag } from '../stringTag';
import { DoubleTag } from '../doubleTag';

export class BusinessProfile {
  businessProfileId: string;
  businessName: string;
  ownerIds: string[];
  stringTags: StringTag[];
  doubleTags: DoubleTag[];
}
