import { Injectable } from '@angular/core';
import { NbToastrService, NbComponentStatus, NbGlobalLogicalPosition } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class ToasterService {
  constructor(private toastrService: NbToastrService) {}

  public showToast(title: string, message: string, status: NbComponentStatus) {
    this.toastrService.show(message, title, {
      status,
      preventDuplicates: true,
      duplicatesBehaviour: 'all',
      duration: 6000,
      position: NbGlobalLogicalPosition.BOTTOM_END,
    });
  }

  public error(title: string, message: string): void {
    this.showToast(title, message, 'danger');
  }
}
