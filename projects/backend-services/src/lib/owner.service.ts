import { Injectable } from '@angular/core';
import { Profile } from './profile/models/profile/profile';
import { PROFILE_KEY } from './session.constants';

@Injectable({
  providedIn: 'root',
})
export class OwnerService {
  constructor() {}

  public getOwner(): string {
    try {
      const profile: Profile = JSON.parse(sessionStorage.getItem(PROFILE_KEY));
      if (profile && profile.profileId) {
        return profile.businessProfile.businessName;
      } else {
        // TODO: So a user can get at least the public available ones.
        return 'suitless';
      }
    } catch {
      return 'suitless';
    }
  }
}
