import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { pluck } from 'rxjs/operators';
import { Config } from '../config';
import { OwnerService } from '../owner.service';
import { ToasterService } from '../toaster.service';
import { ICDNFile } from './models/iCDNFile';
import { ServiceableWrapper } from './models/serviceable-wrapper';

@Injectable({
  providedIn: 'root',
})
export class CdnService {
  private config: Config = new Config();

  constructor(
    private httpClient: HttpClient,
    private toasterService: ToasterService,
    private ownerService: OwnerService,
    @Optional() config?: Config
  ) {
    if (config) {
      this.config = config;
    }
  }

  public uploadUniqueServiceable(file: ICDNFile, owner?: string): Promise<ServiceableWrapper> {
    const name: string = file.name;
    const formData = new FormData();
    formData.set('file', file.data);
    formData.set('fileType', file.mimeType);
    formData.set('tag', name.split('.', 1)[0].replace(' ', '_').concat(Date.now().toString()));
    formData.set('locked', 'false');

    return this.httpClient
      .post(`${this.config.apiUrl}/cdn/${owner ? owner : this.ownerService.getOwner()}`, formData)
      .pipe<ServiceableWrapper>(pluck('inlineServiceableWrapper'))
      .toPromise();
  }

  public uploadServiceable(file: ICDNFile, owner?: string): Promise<ServiceableWrapper> {
    const name: string = file.name;
    const formData = new FormData();
    formData.set('file', file.data);
    formData.set('fileType', file.mimeType);
    formData.set('tag', name.split('.', 1)[0].replace(' ', '_'));
    formData.set('locked', 'false');

    return this.httpClient
      .post(`${this.config.apiUrl}/cdn/${owner ? owner : this.ownerService.getOwner()}`, formData)
      .pipe<ServiceableWrapper>(pluck('inlineServiceableWrapper'))
      .toPromise();
  }

  public updateServiceable(file: ICDNFile): Promise<ServiceableWrapper> {
    const name: string = file.name;
    const formData = new FormData();
    formData.set('file', file.data);
    formData.set('fileType', file.mimeType);
    formData.set('tag', name.split('.', 1)[0].replace(' ', '_'));
    formData.set('locked', 'false');

    return this.httpClient
      .put(`${this.config.apiUrl}/cdn/${this.ownerService.getOwner()}`, formData)
      .pipe<ServiceableWrapper>(pluck('inlineServiceableWrapper'))
      .toPromise();
  }

  public deleteServiceable(tag: string): void {
    this.httpClient
      .delete(`${this.config.apiUrl}/cdn/${this.ownerService.getOwner()}/${tag}`)
      .subscribe(() => this.toasterService.showToast('Bye', 'Deleted successfully!', 'info'));
  }

  public getInlineServiceable(tag: string): string {
    return `${this.config.apiUrl}/cdn/${this.ownerService.getOwner()}/${tag}`;
  }
}
