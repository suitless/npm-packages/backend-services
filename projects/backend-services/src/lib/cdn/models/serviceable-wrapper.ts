export class ServiceableWrapper {
    id: string;
    locked: boolean;
    tag: string;
    type: string;

    constructor(id: string, locked: boolean, tag: string, type: string) {
        this.id = id;
        this.locked = locked;
        this.tag = tag;
        this.type = type;
    }
}
