export interface ICDNFile {
  name: string;
  mimeType: string;
  data: File;
}
