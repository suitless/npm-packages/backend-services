import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { CommonModule } from '@angular/common';
import { Config } from './config';

@NgModule({
  imports: [CommonModule],
})
export class BackendServiceModule {
  constructor(@Optional() @SkipSelf() parentModule?: BackendServiceModule) {
    if (parentModule) {
      throw new Error('Backend-Service module is already loaded. Import it in the AppModule only');
    }
  }

  public static forRoot(config: Config): ModuleWithProviders<BackendServiceModule> {
    return {
      ngModule: BackendServiceModule,
      providers: [{ provide: Config, useValue: config }],
    };
  }
}
