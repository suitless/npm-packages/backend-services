class KeycloakConfig {
  public url: string;
  public realm: string;
  public clientId: string;
}

export class Config {
  public apiUrl: string;
  public keycloak: KeycloakConfig;
}
