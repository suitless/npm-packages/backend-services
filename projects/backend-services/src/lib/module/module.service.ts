import { HttpClient } from '@angular/common/http';
import { Injectable, Optional } from '@angular/core';
import { IModule } from '@suitless/module-domain';
import { KeycloakService } from 'keycloak-angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { Config } from '../config';
import { OwnerService } from '../owner.service';
import { ToasterService } from '../toaster.service';

// const noInterceptHeader = new HttpHeaders().set(InterceptorSkipHeader, '');
const noInterceptHeader = null;

@Injectable({
  providedIn: 'root',
})
export class ModuleService {
  private config: Config = new Config();
  private modulesSubject = new BehaviorSubject<IModule[]>([]);

  constructor(
    private httpClient: HttpClient,
    private keycloakService: KeycloakService,
    private toasterService: ToasterService,
    private ownerService: OwnerService,
    @Optional() config?: Config
  ) {
    if (config) {
      this.config = config;
    }
  }

  public getById(id: string, noErrorStatus: boolean = false): Observable<IModule> {
    const headers = noErrorStatus ? noInterceptHeader : null;
    return this.httpClient.get(`${this.config.apiUrl}/modules/${id}`, { headers }).pipe(pluck('module'));
  }

  public getModuleByName(name: string, version: string): Observable<IModule> {
    return this.httpClient
      .get(`${this.config.apiUrl}/modules/${this.ownerService.getOwner()}/name/${name}/v/${version}`)
      .pipe(pluck('module'));
  }

  public getLatestByName(name: string): Observable<IModule> {
    return this.httpClient
      .get(`${this.config.apiUrl}/modules/${this.ownerService.getOwner()}/name/${name}/latest`)
      .pipe(pluck('module'));
  }

  public get(version: string): Observable<IModule[]> {
    this.httpClient
      .get(
        `${this.config.apiUrl}/modules/${this.ownerService.getOwner()}/v/${version}${
          this.keycloakService.isUserInRole('admin') ? '/private' : ''
        }`
      )
      .pipe(pluck('module'))
      .subscribe((modules: IModule[]) => this.modulesSubject.next(modules));

    return this.modulesSubject.asObservable();
  }

  public getLatest(): Observable<IModule[]> {
    this.httpClient
      .get(
        `${this.config.apiUrl}/modules/${this.ownerService.getOwner()}/latest${
          this.keycloakService.isUserInRole('admin') ? '/private' : ''
        }`
      )
      .pipe(pluck('module'))
      .subscribe((modules: IModule[]) => this.modulesSubject.next(modules));

    return this.modulesSubject.asObservable();
  }

  public getModulesByName(name: string): Observable<IModule[]> {
    this.httpClient
      .get(
        `${this.config.apiUrl}/modules/${this.ownerService.getOwner()}/name/${name}${
          this.keycloakService.isUserInRole('admin') ? '/private' : ''
        }`
      )
      .pipe(pluck('module'))
      .subscribe((modules: IModule[]) => this.modulesSubject.next(modules));

    return this.modulesSubject.asObservable();
  }

  public create(reqModel: IModule): Observable<IModule> {
    return this.httpClient.post(`${this.config.apiUrl}/modules/`, reqModel).pipe(pluck('module'));
  }

  public update(reqModel: IModule): Observable<IModule> {
    return this.httpClient.put(`${this.config.apiUrl}/modules/${reqModel.id}`, reqModel).pipe(pluck('module'));
  }

  public changeAccessibility(id: string, isPublic: boolean): Observable<IModule> {
    const reqModel = class {
      exposed = isPublic;
    };

    return this.httpClient.put(`${this.config.apiUrl}/${id}/access`, reqModel).pipe(pluck('module'));
  }

  public delete(id: string): Observable<IModule[]> {
    this.httpClient.delete(`${this.config.apiUrl}/modules/${id}`).subscribe(() => {
      this.toasterService.showToast('Bye', 'Deleted successfully!', 'info');
      this.modulesSubject.next(this.modulesSubject.value.filter((module) => module.id !== id));
    });
    return this.modulesSubject.asObservable();
  }
}
